(function () {
    'use strict';

    var module = angular.module('app', ['onsen', 'ngStorage', 'ui.utils.masks']);
    var default_path = "http://104.131.14.147/mobihomolog";
    var carrinho = [];
    var produto_interna = null;
    var latitude = 0;
    var longitude = 0;
    var accuracy = 0;
    var usuario;
    var cadastro_usuario = null;
    var produto_temp;
    var pedido_temp;
    var pushNotification;
    var num_pedidos = 0;
    var versao_app = 0;
    var firstInicial = true;
    var acesso_sobre_menu = false;

    module.filter('date_hist1', function($filter)
    {
        return function(input)
        {
            if(input == null){ return ""; }
            var _date = $filter('date')(new Date(Date(input)), 'EEE, dd/MM/yyyy');
            return _date.toUpperCase();
        };
    });
    
    module.filter('date_hist2', function($filter)
    {
        return function(input)
        {
            if(input == null){ return ""; }
            var _date = $filter('date')(new Date(Date(input)), 'HH:mm a');
            return _date.toUpperCase();
        };
    });


    module.controller(
        "AppController",
        function ($scope, $window, $localStorage, $timeout, $http, transformRequestAsFormPost) {

            function onDeviceReady() {
                var config_gps = {
                   enableHighAccuracy: true,
                   timeout: 20000,
                   maximumAge: 18000000
                }

                $window.navigator.geolocation.getCurrentPosition(function (position) {
                    $scope.$apply(function () {
                        latitude = position.coords.latitude;
                        longitude = position.coords.longitude;
                        accuracy = position.coords.accuracy;

                        console.log(latitude + " " + longitude + " " + accuracy);
                    });
                }, function (error) {
                    console.log(error);
                },config_gps);

                getAppVersion(function(version) {
                    console.log('Native App Version: ' + version);
                    versao_app = version;
                });
            }
            
            document.addEventListener("deviceready", onDeviceReady, false);

            $timeout(function () {

                var srt_user = $localStorage.user;
                
                if (srt_user) {

                    usuario = JSON.parse(srt_user);
                    
                    $scope.menu.setMainPage("historico.html", {
                        animation: "none"
                    });
                }

            }, 0);

            $scope.novoCadastro = function () {

                $scope.menu.setAbovePage("cadastro.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            $scope.paginaSobre = function () { 

                acesso_sobre_menu = false;

                $scope.menu.setAbovePage("sobre.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doLogin = function (login) {
                var countUp = function () {
                    $scope.modal2.hide('modal2')
                }

                if (login) {

                    $scope.modal1.show('modal1');

                    var request = $http({
                        method: "post",
                        url: default_path + "/users/service_entregador_login",
                        transformRequest: transformRequestAsFormPost,
                        data: {
                            username: login.usuario,
                            password: login.password
                        }
                    });

                    // Store the data-dump of the FORM scope.
                    request.success(
                        function (data, status) {
                                                        
                            if (data.response)
                            {
                                usuario = data.usuario;

                                $localStorage.user = JSON.stringify(usuario);

                                //console.log($localStorage.user);

                                $scope.modal1.hide('modal1');

                                $scope.menu.setMainPage("historico.html", {
                                    animation: "none"
                                });

                                /*$scope.ons.navigator.pushPage('produtos.html', {
                                    animation: "none"
                                });*/
                            }
                            else
                            {
                                $scope.modal1.hide('modal1');
                                $scope.modal2.show('modal2');
                                $timeout(countUp, 1500);
                            }

                        }

                    ).error(function (data, status, headers, config) {
                        $scope.modal1.hide('modal1');
                        $scope.modal2.hide('modal2');
                        alert("Não foi possível realizar a conexão.");
                    });

                }
                else
                {
                    $scope.modal2.show('modal2');
                    $timeout(countUp, 1500);
                }
            };
        }
    );

    module.controller(
        "MenuController",
        function ($scope, $timeout, $localStorage, $http, mySharedService2) {
            
            $scope.$on('handleBroadcast2', function() {
                $scope.num_pedidos = mySharedService2.message;
            });
        
            $scope.goSobre = function () {

                acesso_sobre_menu = true;

                $scope.menu.setAbovePage("sobre.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            $scope.goHistorico = function () {
                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            $scope.goConfig = function () {
                $scope.menu.setAbovePage("configuracao.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            $scope.goSair = function () {
                $localStorage.$reset();

                $scope.menu.setAbovePage("login.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            mySharedService2.prepForBroadcast(num_pedidos);

        }
    );
    
    module.controller(
        "InicialController",
        function ($scope, $timeout, $http, transformRequestAsFormPost, mySharedService2) {
            
            $scope.num_pedidos = num_pedidos;
            
            $scope.$on('handleBroadcast2', function() {
                $scope.num_pedidos = mySharedService2.message;
            });
            
            if(firstInicial)
            {
                var request = $http({
                    method: "post",
                    url: default_path + "/pedidos/service_get_num_pedidos",
                    transformRequest: transformRequestAsFormPost,
                    data: {
                        entregador_id: usuario.id,
                        user_id: usuario.user_id
                    }
                });

                request.success(
                    function (data) {
                        if (data.response) {
                            $scope.num_pedidos = num_pedidos = data.num_rows;
                            mySharedService2.prepForBroadcast(num_pedidos);
                        } else {
                            $scope.num_pedidos = 0;
                        }
                    }

                ).error(function (data, status, headers, config) {
                    alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");
                });
                
                firstInicial = false;
            
            }
            
            $scope.goPedidoRapido = function () {

                carrinho = [];
                cadastro_usuario = null;
                //carrinho[carrinho.length] = produto_temp;

                $scope.menu.setAbovePage("finalizar.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.goProdutos = function () {

                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.goPedido = function () {

                $scope.menu.setAbovePage("pedido.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doConfig = function () {

                $scope.menu.setAbovePage("configuracao.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
        }
    );
    
    module.controller(
        "FinalizarController",
        function ($scope, $timeout, $http, transformRequestAsFormPost) {

            $scope.msg = "Pedido enviado com sucesso.";
            
            var carrinho_interna = pedido_temp.produtos;
            
            var total_cart = 0;

            for (var v = 0; v < carrinho_interna.length; v++) {
                total_cart += (parseFloat(carrinho_interna[v].preco_praticado)*carrinho_interna[v].quantidade);
                carrinho_interna[v].codigo = carrinho_interna[v].produto[0].codigo;
                carrinho_interna[v].nome = carrinho_interna[v].produto[0].nome;
                carrinho_interna[v].volume = carrinho_interna[v].produto[0].volume;
                carrinho_interna[v].peso = carrinho_interna[v].produto[0].peso;
                carrinho_interna[v].composto = eval(carrinho_interna[v].composto);
            }

            $scope.produtos = carrinho_interna;
            $scope.troco = pedido_temp.troco;
            $scope.clientes = pedido_temp.cliente;
            $scope.valor_total = total_cart;
            $scope.pedido = pedido_temp;
            
            
            $http({
                method: 'POST',
                url: default_path + "/produtos/service_pagamentos"
            }).success(function (data) {
                for (var v = 0; v < data.length; v++) {
                    if(data[v].id == pedido_temp.condicao_pagamento_id){
                       $scope.pagamento = data[v].nome;
                    }
                }
                
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            });

            $scope.goCarrinho = function () {
                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            
            var enviarDados = function (id_dados, status_dados) {
                
                $http({
                    method: 'POST',
                    url: default_path + "/pedidos/service_atualizar_entrega",
                    transformRequest: transformRequestAsFormPost,
                    data: {
                        user_id: usuario.user_id,
                        pedido_id: id_dados,
                        pedido_status_id: status_dados
                    }
                }).success(function (data) {

                    if (data.response) {
                        
                        
                        alert("Pedido atualizado");
                                
                        $scope.menu.setAbovePage("historico.html", {
                            animation: "none",
                            closeMenu: true
                        });
                                
                    } else {

                        alert("Falha na atualização do pedido");

                    }

                }).error(function (data, status, headers, config) {
                    alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                    $scope.menu.setAbovePage("historico.html", {
                        animation: "none",
                        closeMenu: true
                    });
                });
            };
            
            
            $scope.goPhone = function (url_access) {
                window.open("tel:"+url_access, '_system');
            };

            $scope.finalizarEntrega = function (dados) {
                enviarDados(dados.id, 9);
            };
            
            $scope.redirecionarEntrega = function (dados) {
                enviarDados(dados.id, 1);
            };

            $scope.fazerEntrega = function (dados) {
                enviarDados(dados.id, 4);
            };

            $scope.recusarEntrega = function (dados) {
                enviarDados(dados.id, 2);
            };

            $scope.naoEncontrado = function (dados) {
                enviarDados(dados.id, 8);
            };

        }
    );
    
     module.controller(
        "HistoricoController",
        function ($scope, $timeout, $http, transformRequestAsFormPost, mySharedService2) {
    
            
            var atualizar_func = function () {

                $timeout(function () {
                    $scope.modal_pedidos.show('modal_pedidos');
                }, 0);
                
                $scope.pedidos = [];
                
                
                var pedidosPendentes = function (ped_pend) {
                    
                    var numpedped = 0;
                    
                    for (var v = 0; v < ped_pend.length; v++) {
                        if(ped_pend[v].pedido_status_id == 1 || ped_pend[v].pedido_status_id == 3 || ped_pend[v].pedido_status_id == 4){
                           numpedped++;
                        }
                    }
                    
                    return numpedped;
                    
                };
                                
                var request = $http({
                            method: "post",
                            url: default_path + "/pedidos/service_entregador_pedidos",
                            transformRequest: transformRequestAsFormPost,
                            data: {
                                entregador_id: usuario.id,
                                user_id: usuario.user_id
                            }
                        });

                request.success(
                    function (data) {
                        $scope.modal_pedidos.hide('modal_pedidos');
                        if (data.response) {
                            $scope.pedidos = data.pedidos;
                            num_pedidos = pedidosPendentes(data.pedidos);
                            mySharedService2.prepForBroadcast(num_pedidos);
                        } else {
                            num_pedidos = 0;
                            mySharedService2.prepForBroadcast(num_pedidos);
                            alert("Sem pedidos para sua lista.");
                        }
                    }

                ).error(function (data, status, headers, config) {
                    alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                    $scope.menu.setAbovePage("historico.html", {
                        animation: "none",
                        closeMenu: true
                    });
                });

            };
            
            $scope.doUpdate = function () {
                atualizar_func();
            };
            
            $scope.goInterna = function (pedido) {
                pedido_temp = pedido;
                
                $scope.menu.setAbovePage("finalizar.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            atualizar_func();
        }
    );
    
    module.controller(
        "SobreController",
        function ($scope, $timeout, $http) {
            
            $scope.versao = versao_app;
            $scope.acesso_sobre_menu = acesso_sobre_menu;
            $scope.nome_fantasia = "Carregando..."
            
            $http({
                method: 'POST',
                url: default_path + "/representantes/service_list"
            }).success(function (data) {
                
                if(data.response)
                {
                    $scope.nome_fantasia = data.sobre.nome_fantasia;
                    $scope.sobre = data.sobre.sobre;
                    $scope.website = data.sobre.website;
                    $scope.facebook = data.sobre.facebook;
                    $scope.twitter = data.sobre.twitter;
                }
                
            }).error(function(data, status, headers, config) {
                
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");
               
                $scope.menu.setAbovePage("login.html", {
                    animation: "none",
                    closeMenu: true
                });
            });
            
            $scope.onBack = function () {

                $scope.menu.setAbovePage("login.html", {
                    animation: "none",
                    closeMenu: true
                });

            };
        }
    );

    module.controller(
        "ConfigController",
        function ($scope, $timeout, $localStorage, $http, transformRequestAsFormPost) {
            
            $scope.doLogout = function (produto) {
                $localStorage.$reset();
                $scope.menu.setAbovePage("login.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            $scope.paginaSobre = function () {                
                $scope.menu.setAbovePage("sobre.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.onBack = function () {
                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );

    module.controller(
        "PedidoController",
        function ($scope, $timeout, $localStorage, $http, transformRequestAsFormPost) {
            
            $http({
                method: 'POST',
                url: default_path + "/produtos/service_pagamentos"
            }).success(function (data) {
                $scope.pagamentos = data; // response data 
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            });
                        
            if(cadastro_usuario != null){
                $scope.cadastro = cadastro_usuario;
            }
            
            $scope.doEnvio = function (cadastro) {

                if (cadastro) {
                
                    if(!cadastro.nome){
                        alert("Por favor, preencha o campo nome");
                    }else if(!cadastro.telefone && !cadastro.celular){
                        alert("Por favor, pelo menos um campo telefônico");
                    }else{
                        cadastro_usuario = cadastro;

                        $scope.menu.setAbovePage("produtos.html", {
                            animation: "none",
                            closeMenu: true
                        });
                    }
                
                }else{
                    alert("Por favor, preencha todos os campos");
                }
                
            };
            
            $scope.onBack = function () {
                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );
    
    module.controller(
        "ProdutosController",
        function ($scope, $timeout, $http, mySharedService) {
            
            
            $scope.$on('handleBroadcast', function() {
                $scope.num_produtos = mySharedService.message;
            });

            $scope.goCarrinho = function () {

                $scope.menu.setAbovePage("carrinho.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doInterna = function (produto) {

                produto_interna = produto;

                $scope.menu.setAbovePage("interna.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $timeout(function () {
                $scope.modal5.show('modal5');
            }, 0);

            $http({
                method: 'POST',
                url: default_path + "/produtos/service_list"
            }).success(function (data) {
                $scope.produtos = data; // response data 
                $scope.modal5.hide('modal5');
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            });
            
            $scope.onBack = function () {
                $scope.menu.setAbovePage("pedido.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            mySharedService.prepForBroadcast(carrinho.length);

        }
    );
    
    module.controller(
        "InternaController",
        function ($scope, $timeout, $http, mySharedService) {

            $scope.produto = produto_interna;

            $scope.onBack = function () {

                $scope.menu.setAbovePage("produtos.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            $scope.doPedido = function () {
                carrinho[carrinho.length] = $scope.produto;
                
                mySharedService.prepForBroadcast(carrinho.length);
                
                $scope.menu.setAbovePage("produtos.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );
    
    module.controller(
        "CarrinhoController",
        function ($scope, $timeout, $http, transformRequestAsFormPost, mySharedService) {

            $scope.produtos = carrinho;

            $scope.goPedido = function (produto) {

                $scope.menu.setAbovePage("finalizar_pedido.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doDelete = function (produto) {

                var aux_cart = [];

                for (var i = 0; i < carrinho.length; i++) {

                    if (carrinho[i].id != produto.id) {
                        aux_cart[aux_cart.length] = carrinho[i];
                    }

                }

                carrinho = aux_cart;
                
                mySharedService.prepForBroadcast(carrinho.length);
                
                if(carrinho.length > 0){
                    $scope.menu.setAbovePage("carrinho.html", {
                        animation: "none",
                        closeMenu: true
                    });
                }else{
                    $scope.menu.setAbovePage("produtos.html", {
                        animation: "none",
                        closeMenu: true
                    });
                }
            };
            
            $scope.onBack = function () {
                $scope.menu.setAbovePage("produtos.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            
            mySharedService.prepForBroadcast(carrinho.length);
            
        }
    );
    
    module.controller(
        "FinalizarPedidoController",
        function ($scope, $timeout, $http, transformRequestAsFormPost) {

            var troco_calculado;
            
            $scope.calcularTroco = function () {
                troco_calculado = parseFloat($scope.valor_pago)-parseFloat($scope.valor_total);
                $scope.troco_calculado = troco_calculado>0?troco_calculado:0;
            };
            
            $scope.produtos = carrinho;
            $scope.msg = "Cadastro e venda enviada com sucesso.";

            var total_cart = 0;

            for (var v = 0; v < carrinho.length; v++) {
                total_cart += parseFloat(carrinho[v].preco);
            }

            $scope.valor_total = total_cart;

            $http({
                method: 'POST',
                url: default_path + "/produtos/service_pagamentos"
            }).success(function (data) {
                $scope.pagamentos = data; // response data 
                $scope.finalizar = {
                    pagamento_id: 1
                };
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            });

            $scope.goCarrinho = function () {
                
                $scope.menu.setAbovePage("carrinho.html", {
                    animation: "none",
                    closeMenu: true
                });
                
            };

            $scope.fimPedido = function (dados) {


                var countDown = function () {
                    $scope.modal7.hide('modal7')
                }

                var countUp = function () {
                    $scope.modal7.hide('modal7')
                    carrinho = [];
                    cadastro_usuario = null;

                    $scope.menu.setAbovePage("historico.html", {
                        animation: "none",
                        closeMenu: true
                    });
                }


                if (carrinho.length) {
                    
                    
                    if(troco_calculado >=0 && $scope.valor_pago){

                        $scope.modal6.show('modal6');

                        var send_user = usuario;
                        send_user.condicao_pagamento_id = dados.pagamento_id;
                        send_user.troco = troco_calculado;

                        console.log(send_user);

                        var request = $http({
                            method: "post",
                            url: default_path + "/pedidos/service_insert_entregador",
                            transformRequest: transformRequestAsFormPost,
                            data: {
                                json_data: JSON.stringify(carrinho),
                                json_usuario: JSON.stringify(send_user),
                                json_cadastro: JSON.stringify(cadastro_usuario),
                                lat: latitude,
                                lng: longitude,
                                acc: accuracy
                            }
                        });

                        request.success(
                            function (data) {
                                if (data.response) {
                                    $scope.msg = data.msg;
                                    $scope.modal6.hide('modal6');
                                    $scope.modal7.show('modal7');
                                    $timeout(countUp, 2000);
                                } else {
                                    $scope.msg = data.msg;
                                    $scope.modal6.hide('modal6');
                                    $scope.modal7.show('modal7');
                                    $timeout(countDown, 2000);
                                }

                            }

                        ).error(function (data, status, headers, config) {
                            alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                            $scope.menu.setAbovePage("historico.html", {
                                animation: "none",
                                closeMenu: true
                            });
                        });
                        
                    }else{
                        alert("O campo valor pago não foi informado ou o menor que o valor total.");
                    }

                } else {
                    $scope.msg = "Você não selecionou um produto.";
                    $scope.modal7.show('modal7');
                    $timeout(countDown, 2000);
                }  
            };
        }
    );

    angular.module('app').filter('moment', function () {
      return function (input, momentFn /*, param1, param2, etc... */) {
        momentObj = moment(input);
        momentObj.locale('pt-br');
        var args = Array.prototype.slice.call(arguments, 2),momentObj;
        return momentObj[momentFn].apply(momentObj, args);
      };
    });
    
    module.factory('mySharedService', function($rootScope) {
        var sharedService = {};

        sharedService.message = '';

        sharedService.prepForBroadcast = function(msg) {
            this.message = msg;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function() {
            $rootScope.$broadcast('handleBroadcast');
        };

        return sharedService;
    });
    
    module.factory('mySharedService2', function($rootScope) {
        var sharedService = {};

        sharedService.message = '';

        sharedService.prepForBroadcast = function(msg) {
            this.message = msg;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function() {
            $rootScope.$broadcast('handleBroadcast2');
        };

        return sharedService;
    });

    module.factory(
        "transformRequestAsFormPost",
        function () {

            function transformRequest(data, getHeaders) {

                var headers = getHeaders();

                headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";

                return (serializeData(data));

            }

            return (transformRequest);

            // ---
            // PRVIATE METHODS.
            // ---
            function serializeData(data) {

                if (!angular.isObject(data)) {

                    return ((data == null) ? "" : data.toString());
                }
                var buffer = [];

                for (var name in data) {

                    if (!data.hasOwnProperty(name)) {
                        continue;
                    }

                    var value = data[name];

                    buffer.push(
                        encodeURIComponent(name) +
                        "=" +
                        encodeURIComponent((value == null) ? "" : value)
                    );

                }

                var source = buffer
                    .join("&")
                    .replace(/%20/g, "+");

                return (source);

            }

        }
    );


})();