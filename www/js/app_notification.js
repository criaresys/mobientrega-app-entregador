(function () {
    'use strict';

    var module = angular.module('app', ['onsen', 'ngStorage', 'ui.utils.masks']);
    //var default_path = "http://criaresys.com.br/mariscao";
    var default_path = "http://criaresys.com.br/mobigas";
    //var default_path = "http://localhost:8080/mobigas/";
    var carrinho = [];
    var produto_interna = null;
    var latitude = 0;
    var longitude = 0;
    var accuracy = 0;
    var usuario;
    var cadastro_usuario = null;
    var produto_temp;
    var pedido_temp;
    var pushNotification;
    
    module.filter('date_hist1', function($filter)
    {
        return function(input)
        {
            if(input == null){ return ""; }
            var _date = $filter('date')(new Date(input), 'EEE, dd/MM/yyyy');
            return _date.toUpperCase();
        };
    });
    
    module.filter('date_hist2', function($filter)
    {
        return function(input)
        {
            if(input == null){ return ""; }
            var _date = $filter('date')(new Date(input), 'HH:mm a');
            return _date.toUpperCase();
        };
    });


    module.controller(
        "AppController",
        function ($scope, $window, $localStorage, $timeout, $http, transformRequestAsFormPost) {

            //console.log($localStorage.user);
            
            //project id: 205699086329
            //server apikey AIzaSyD895I3tsYuMqJMkKbaQ9NOg5PyoW6t96M
            
            
            function onDeviceReady() {
                
                pushNotification = window.plugins.pushNotification;
                
                
                function onNotificationAPN (event) {
                    if ( event.alert )
                    {
                        navigator.notification.alert(event.alert);
                    }

                    if ( event.sound )
                    {
                        var snd = new Media(event.sound);
                        snd.play();
                    }

                    if ( event.badge )
                    {
                        pushNotification.setApplicationIconBadgeNumber(successHandler, errorHandler, event.badge);
                    }
                }

                // Android
                function onNotificationGCM(e) {
                    //jQuery("#app-status-ul").append('<li>EVENT -> RECEIVED:' + e.event + '</li>');

                    switch( e.event )
                    {
                    case 'registered':
                        if ( e.regid.length > 0 )
                        {
                            //$("#app-status-ul").append('<li>REGISTERED -> REGID:' + e.regid + "</li>");
                            // Your GCM push server needs to know the regID before it can push to this device
                            // here is where you might want to send it the regID for later use.
                            console.log("regID = " + e.regid);
                        }
                    break;

                    case 'message':
                        // if this flag is set, this notification happened while we were in the foreground.
                        // you might want to play a sound to get the user's attention, throw up a dialog, etc.
                        if ( e.foreground )
                        {
                            //$("#app-status-ul").append('<li>--INLINE NOTIFICATION--' + '</li>');

                            // if the notification contains a soundname, play it.
                            //var my_media = new Media("/android_asset/www/"+e.soundname);
                            //my_media.play();
                        }
                        else
                        {  // otherwise we were launched because the user touched a notification in the notification tray.
                            if ( e.coldstart )
                            {
                                //$("#app-status-ul").append('<li>--COLDSTART NOTIFICATION--' + '</li>');
                            }
                            else
                            {
                                //$("#app-status-ul").append('<li>--BACKGROUND NOTIFICATION--' + '</li>');
                            }
                        }

                        //$("#app-status-ul").append('<li>MESSAGE -> MSG: ' + e.payload.message + '</li>');
                        //$("#app-status-ul").append('<li>MESSAGE -> MSGCNT: ' + e.payload.msgcnt + '</li>');
                    break;

                    case 'error':
                        //$("#app-status-ul").append('<li>ERROR -> MSG:' + e.msg + '</li>');
                    break;

                    default:
                        //$("#app-status-ul").append('<li>EVENT -> Unknown, an event was received and we do not know what it is</li>');
                    break;
                  }
                }
                
                function successHandler (result) {
                    alert('result = ' + result);
                }
                
                function errorHandler (error) {
                    alert('error = ' + error);
                }
                
                function tokenHandler (result) {
                    // Your iOS push server needs to know the token before it can push to this device
                    // here is where you might want to send it the token for later use.
                    alert('device token = ' + result);
                }
                
                if ( device.platform == 'android' || device.platform == 'Android' )
                {
                    pushNotification.register(
                        successHandler,
                        errorHandler, {
                            "senderID":"205699086329",
                            "ecb":"onNotificationGCM"
                        });
                }
                else
                {
                    pushNotification.register(
                        tokenHandler,
                        errorHandler, {
                            "badge":"true",
                            "sound":"true",
                            "alert":"true",
                            "ecb":"onNotificationAPN"
                        });
                }
                
                var config_gps = {
                   enableHighAccuracy: true,
                   timeout: 20000,
                   maximumAge: 18000000
                }

                $window.navigator.geolocation.getCurrentPosition(function (position) {
                    $scope.$apply(function () {
                        latitude = position.coords.latitude;
                        longitude = position.coords.longitude;
                        accuracy = position.coords.accuracy;

                        console.log(latitude + " " + longitude + " " + accuracy);
                    });
                }, function (error) {
                    console.log(error);
                },config_gps);
            }
            
            document.addEventListener("deviceready", onDeviceReady, false);

            $timeout(function () {

                var srt_user = $localStorage.user;

                if (srt_user) {

                    usuario = JSON.parse(srt_user);

                    $scope.menu.setMainPage("inicial.html", {
                        animation: "none"
                    });
                }

            }, 0);

            $scope.novoCadastro = function () {

                $scope.menu.setAbovePage("cadastro.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doLogin = function (login) {
                var countUp = function () {
                    $scope.modal2.hide('modal2')
                }

                if (login) {

                    $scope.modal1.show('modal1');

                    var request = $http({
                        method: "post",
                        url: default_path + "/users/service_entregador_login",
                        transformRequest: transformRequestAsFormPost,
                        data: {
                            username: login.usuario,
                            password: login.password
                        }
                    });

                    // Store the data-dump of the FORM scope.
                    request.success(
                        function (data, status) {
                                                        
                            if (data.response) {

                                usuario = data.usuario;

                                $localStorage.user = JSON.stringify(usuario);

                                //console.log($localStorage.user);

                                $scope.modal1.hide('modal1');

                                $scope.menu.setMainPage("inicial.html", {
                                    animation: "none"
                                });

                                /*$scope.ons.navigator.pushPage('produtos.html', {
                                    animation: "none"
                                });*/
                            } else {
                                $scope.modal1.hide('modal1');
                                $scope.modal2.show('modal2');
                                $timeout(countUp, 1500);
                            }

                        }

                    ).error(function (data, status, headers, config) {
                        $scope.modal1.hide('modal1');
                        $scope.modal2.hide('modal2');
                        alert("Não foi possível realizar a conexão.");
                    });

                } else {
                    $scope.modal2.show('modal2');
                    $timeout(countUp, 1500);
                }
            };
        });

    module.controller(
        "MenuController",
        function ($scope, $timeout, $localStorage, $http) {

            $scope.num_produtos = carrinho.length;

            $scope.goInicio = function () {
                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            $scope.goHistorico = function () {
                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            $scope.goSair = function () {
                $localStorage.$reset();

                $scope.menu.setAbovePage("login.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );



    module.controller(
        "InicialController",
        function ($scope, $timeout, $http) {
            
            //console.log(usuario);

            $scope.goPedidoRapido = function () {

                carrinho = [];
                cadastro_usuario = null;
                //carrinho[carrinho.length] = produto_temp;

                $scope.menu.setAbovePage("finalizar.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.goProdutos = function () {

                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.goPedido = function () {

                $scope.menu.setAbovePage("pedido.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doConfig = function () {

                $scope.menu.setAbovePage("configuracao.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );




    module.controller(
        "FinalizarController",
        function ($scope, $timeout, $http, transformRequestAsFormPost) {

            $scope.msg = "Pedido enviado com sucesso.";
            
            carrinho = pedido_temp.produtos;
            
            var total_cart = 0;

            for (var v = 0; v < carrinho.length; v++) {
                total_cart += parseFloat(carrinho[v].preco_praticado);
                carrinho[v].codigo = carrinho[v].produto[0].codigo;
                carrinho[v].nome = carrinho[v].produto[0].nome;
                carrinho[v].volume = carrinho[v].produto[0].volume;
                carrinho[v].peso = carrinho[v].produto[0].peso;
            }

            $scope.produtos = carrinho;
            $scope.troco = pedido_temp.troco;
            $scope.clientes = pedido_temp.cliente;
            $scope.valor_total = total_cart;
            $scope.pedido = pedido_temp;
            
            $http({
                method: 'POST',
                url: default_path + "/produtos/service_pagamentos"
            }).success(function (data) {
                for (var v = 0; v < data.length; v++) {
                    if(data[v].id == pedido_temp.condicao_pagamento_id){
                       $scope.pagamento = data[v].nome;
                    }
                }
                
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            });

            $scope.goCarrinho = function () {
                $scope.menu.setAbovePage("historico.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            
            var enviarDados = function (id_dados, status_dados) {
                
                $http({
                    method: 'POST',
                    url: default_path + "/pedidos/service_atualizar_entrega",
                    transformRequest: transformRequestAsFormPost,
                    data: {
                        user_id: usuario.user_id,
                        pedido_id: id_dados,
                        pedido_status_id: status_dados
                    }
                }).success(function (data) {

                    if (data.response) {
                        
                        
                        alert("Pedido atualizado");
                                
                        $scope.menu.setAbovePage("historico.html", {
                            animation: "none",
                            closeMenu: true
                        });
                                
                    } else {

                        alert("Falha na atualização do pedido");

                    }

                }).error(function (data, status, headers, config) {
                    alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                    $scope.menu.setAbovePage("inicial.html", {
                        animation: "none",
                        closeMenu: true
                    });
                });
            };
            

            $scope.finalizarEntrega = function (dados) {
                enviarDados(dados.id, 9);
            };
            
            $scope.redirecionarEntrega = function (dados) {
                enviarDados(dados.id, 1);
            };

            $scope.fazerEntrega = function (dados) {
                enviarDados(dados.id, 4);
            };

            $scope.recusarEntrega = function (dados) {
                enviarDados(dados.id, 2);
            };

            $scope.naoEncontrado = function (dados) {
                enviarDados(dados.id, 8);
            };

        }
    );
    
     module.controller(
        "HistoricoController",
        function ($scope, $timeout, $http, transformRequestAsFormPost) {
    
            
            var atualizar_func = function () {

                $timeout(function () {
                    $scope.modal_pedidos.show('modal_pedidos');
                }, 0);
                
                $scope.pedidos = [];
                                
                var request = $http({
                            method: "post",
                            url: default_path + "/pedidos/service_entregador_pedidos",
                            transformRequest: transformRequestAsFormPost,
                            data: {
                                entregador_id: usuario.id,
                                user_id: usuario.user_id
                            }
                        });

                request.success(
                    function (data) {
                        $scope.modal_pedidos.hide('modal_pedidos');
                        if (data.response) {
                            $scope.pedidos = data.pedidos;
                        } else {
                            alert("Sem pedidos para sua lista.");
                        }
                    }

                ).error(function (data, status, headers, config) {
                    alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                    $scope.menu.setAbovePage("inicial.html", {
                        animation: "none",
                        closeMenu: true
                    });
                });

            };
            
            $scope.doUpdate = function () {
                atualizar_func();
            };
            
            $scope.goInterna = function (pedido) {
                pedido_temp = pedido;
                
                $scope.menu.setAbovePage("finalizar.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            atualizar_func();
        }
    );

    module.controller(
        "ConfigController",
        function ($scope, $timeout, $localStorage, $http, transformRequestAsFormPost) {
            
            $scope.doLogout = function (produto) {
                $localStorage.$reset();
                $scope.menu.setAbovePage("login.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.onBack = function () {
                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );

    module.controller(
        "PedidoController",
        function ($scope, $timeout, $localStorage, $http, transformRequestAsFormPost) {
            
            $http({
                method: 'POST',
                url: default_path + "/produtos/service_pagamentos"
            }).success(function (data) {
                $scope.pagamentos = data; // response data 
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            });
                        
            if(cadastro_usuario != null){
                $scope.cadastro = cadastro_usuario;
            }
            
            $scope.doEnvio = function (cadastro) {

                if (cadastro) {
                
                    if(!cadastro.nome){
                        alert("Por favor, preencha o campo nome");
                    }else if(!cadastro.telefone && !cadastro.celular){
                        alert("Por favor, pelo menos um campo telefônico");
                    }else{
                        cadastro_usuario = cadastro;

                        $scope.menu.setAbovePage("produtos.html", {
                            animation: "none",
                            closeMenu: true
                        });
                    }
                
                }else{
                    alert("Por favor, preencha todos os campos");
                }
                
            };
            
            $scope.onBack = function () {
                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );
    
    
    


    module.controller(
        "ProdutosController",
        function ($scope, $timeout, $http, mySharedService) {
            
            
            $scope.$on('handleBroadcast', function() {
                $scope.num_produtos = mySharedService.message;
            });

            $scope.goCarrinho = function () {

                $scope.menu.setAbovePage("carrinho.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doInterna = function (produto) {

                produto_interna = produto;

                $scope.menu.setAbovePage("interna.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $timeout(function () {
                $scope.modal5.show('modal5');
            }, 0);

            $http({
                method: 'POST',
                url: default_path + "/produtos/service_list"
            }).success(function (data) {
                $scope.produtos = data; // response data 
                $scope.modal5.hide('modal5');
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            });
            
            $scope.onBack = function () {
                $scope.menu.setAbovePage("pedido.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            mySharedService.prepForBroadcast(carrinho.length);

        }
    );
    
    module.controller(
        "InternaController",
        function ($scope, $timeout, $http, mySharedService) {

            $scope.produto = produto_interna;

            $scope.onBack = function () {

                $scope.menu.setAbovePage("produtos.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            $scope.doPedido = function () {
                carrinho[carrinho.length] = $scope.produto;
                
                mySharedService.prepForBroadcast(carrinho.length);
                
                $scope.menu.setAbovePage("produtos.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

        }
    );
    
    
    module.controller(
        "CarrinhoController",
        function ($scope, $timeout, $http, transformRequestAsFormPost, mySharedService) {

            $scope.produtos = carrinho;

            $scope.goPedido = function (produto) {

                $scope.menu.setAbovePage("finalizar_pedido.html", {
                    animation: "none",
                    closeMenu: true
                });
            };

            $scope.doDelete = function (produto) {

                var aux_cart = [];

                for (var i = 0; i < carrinho.length; i++) {

                    if (carrinho[i].id != produto.id) {
                        aux_cart[aux_cart.length] = carrinho[i];
                    }

                }

                carrinho = aux_cart;
                
                mySharedService.prepForBroadcast(carrinho.length);
                
                if(carrinho.length > 0){
                    $scope.menu.setAbovePage("carrinho.html", {
                        animation: "none",
                        closeMenu: true
                    });
                }else{
                    $scope.menu.setAbovePage("produtos.html", {
                        animation: "none",
                        closeMenu: true
                    });
                }
            };
            
            $scope.onBack = function () {
                $scope.menu.setAbovePage("produtos.html", {
                    animation: "none",
                    closeMenu: true
                });
            };
            
            
            mySharedService.prepForBroadcast(carrinho.length);
            
        }
    );
    
    
    
    module.controller(
        "FinalizarPedidoController",
        function ($scope, $timeout, $http, transformRequestAsFormPost) {

            var troco_calculado;
            
            $scope.calcularTroco = function () {
                troco_calculado = parseFloat($scope.valor_pago)-parseFloat($scope.valor_total);
                $scope.troco_calculado = troco_calculado>0?troco_calculado:0;
            };
            
            $scope.produtos = carrinho;
            $scope.msg = "Cadastro e venda enviada com sucesso.";

            var total_cart = 0;

            for (var v = 0; v < carrinho.length; v++) {
                total_cart += parseFloat(carrinho[v].preco);
            }

            $scope.valor_total = total_cart;

            $http({
                method: 'POST',
                url: default_path + "/produtos/service_pagamentos"
            }).success(function (data) {
                $scope.pagamentos = data; // response data 
                $scope.finalizar = {
                    pagamento_id: 1
                };
            }).error(function (data, status, headers, config) {
                alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                $scope.menu.setAbovePage("inicial.html", {
                    animation: "none",
                    closeMenu: true
                });
            });

            $scope.goCarrinho = function () {
                
                $scope.menu.setAbovePage("carrinho.html", {
                    animation: "none",
                    closeMenu: true
                });
                
            };

            $scope.fimPedido = function (dados) {


                var countDown = function () {
                    $scope.modal7.hide('modal7')
                }

                var countUp = function () {
                    $scope.modal7.hide('modal7')
                    carrinho = [];
                    cadastro_usuario = null;

                    $scope.menu.setAbovePage("inicial.html", {
                        animation: "none",
                        closeMenu: true
                    });
                }


                if (carrinho.length) {
                    
                    
                    if(troco_calculado >=0 && $scope.valor_pago){

                        $scope.modal6.show('modal6');

                        var send_user = usuario;
                        send_user.condicao_pagamento_id = dados.pagamento_id;
                        send_user.troco = troco_calculado;

                        console.log(send_user);

                        var request = $http({
                            method: "post",
                            url: default_path + "/pedidos/service_insert_entregador",
                            transformRequest: transformRequestAsFormPost,
                            data: {
                                json_data: JSON.stringify(carrinho),
                                json_usuario: JSON.stringify(send_user),
                                json_cadastro: JSON.stringify(cadastro_usuario),
                                lat: latitude,
                                lng: longitude,
                                acc: accuracy
                            }
                        });

                        request.success(
                            function (data) {
                                if (data.response) {
                                    $scope.msg = data.msg;
                                    $scope.modal6.hide('modal6');
                                    $scope.modal7.show('modal7');
                                    $timeout(countUp, 2000);
                                } else {
                                    $scope.msg = data.msg;
                                    $scope.modal6.hide('modal6');
                                    $scope.modal7.show('modal7');
                                    $timeout(countDown, 2000);
                                }

                            }

                        ).error(function (data, status, headers, config) {
                            alert("O aplicativo não conseguiu conexão. Verifique a sua internet.");

                            $scope.menu.setAbovePage("inicial.html", {
                                animation: "none",
                                closeMenu: true
                            });
                        });
                        
                    }else{
                        alert("O campo valor pago não foi informado ou o menor que o valor total.");
                    }

                } else {
                    $scope.msg = "Você não selecionou um produto.";
                    $scope.modal7.show('modal7');
                    $timeout(countDown, 2000);
                }

                
            };

        }
    );
    
    
    
    
    module.factory('mySharedService', function($rootScope) {
        var sharedService = {};

        sharedService.message = '';

        sharedService.prepForBroadcast = function(msg) {
            this.message = msg;
            this.broadcastItem();
        };

        sharedService.broadcastItem = function() {
            $rootScope.$broadcast('handleBroadcast');
        };

        return sharedService;
    });

    module.factory(
        "transformRequestAsFormPost",
        function () {

            function transformRequest(data, getHeaders) {

                var headers = getHeaders();

                headers["Content-Type"] = "application/x-www-form-urlencoded; charset=utf-8";

                return (serializeData(data));

            }

            return (transformRequest);

            // ---
            // PRVIATE METHODS.
            // ---
            function serializeData(data) {

                if (!angular.isObject(data)) {

                    return ((data == null) ? "" : data.toString());
                }
                var buffer = [];

                for (var name in data) {

                    if (!data.hasOwnProperty(name)) {
                        continue;
                    }

                    var value = data[name];

                    buffer.push(
                        encodeURIComponent(name) +
                        "=" +
                        encodeURIComponent((value == null) ? "" : value)
                    );

                }

                var source = buffer
                    .join("&")
                    .replace(/%20/g, "+");

                return (source);

            }

        }
    );


})();